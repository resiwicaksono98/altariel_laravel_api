<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class ProjectController extends Controller
{
  public function index()
  {
    $res = Http::get('https://private-anon-bd2d1575dd-testaltariel.apiary-mock.com/projects');
    $projects = $res->json();
    return view('project', compact('projects'));
  }
}
