<?php

use App\Http\Controllers\ProjectController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
  return view('home');
});
Route::get('/contact', function () {
  return view('contact');
});
Route::get('/about', function () {
  return view('about');
});
Route::get('/project', [ProjectController::class, 'index']);
Route::get('/service', function () {
  return view('service');
});
