      <!-- Footer -->
      <footer class="text-center text-lg-start text-white p-4">
          <div class="row mt-4">
              <div class="col-md-4 d-flex justify-content-center align-items-center">
                  <div class="mx-4">
                      <div class="title">JOSH ARCHITECTURE</div>
                  </div>
              </div>
              <hr class="my-3 border border-1 border-white d-md-none w-75 m-auto" />
              <div class="col-md-4">
                  <div class="mx-4">
                      <div class="head-title">Headquarters</div>
                      <div class="my-4 content-footer">17th Sesame Street, Upper East High, New York, USA Postal Code
                          127658</div>
                  </div>
              </div>
              <div class="col-md-4">
                  <div class="mx-4">
                      <div class="fs-5">Contact Us</div>
                      <div class="d-flex mt-3 gap-4 justify-content-center justify-content-md-start">
                          <img src="./assets/icons/email.png" class="img-fluid rounded-top" alt="" />
                          <img src="./assets/icons/whatsapp.png" class="img-fluid rounded-top" alt="" />
                          <img src="./assets/icons/instagram.png" class="img-fluid rounded-top" alt="" />
                          <img src="./assets/icons/twitter.png" class="img-fluid rounded-top" alt="" />
                          <img src="./assets/icons/facebook.png" class="img-fluid rounded-top" alt="" />
                      </div>
                  </div>
              </div>
          </div>
          <!-- footer -->
          <div class="footer mb-2">
              <div class="d-flex justify-content-between">
                  <div class="d-none d-md-block">© 2022-2023, All Rights Reserved</div>
                  <div class="d-block d-md-none">© All Rights Reserved</div>
                  <div class="d-flex gap-3 gap-md-5">
                      <div>Privacy Policy</div>
                      <div>Terms & Conditions</div>
                  </div>
              </div>
          </div>
      </footer>
      <!-- Footer -->
