  <!-- Navbar -->
  <nav class="navbar navbar-expand-lg bg-light">
      <div class="container">
          <a class="navbar-brand" href="/">JOSH ARCHITECTURE</a>
          <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent"
              aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarSupportedContent">
              <ul class="navbar-nav ms-auto mb-2 mb-lg-0 md-gap-5 flex">
                  <div class="d-flex justify-content-end p-4 d-md-none">
                      <button type="button" class="btn-close btn-close-white" data-bs-toggle="collapse"
                          data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                          aria-expanded="false" aria-label="Toggle navigation"></button>
                  </div>
                  <li class="nav-item">
                      <a class="nav-link" href="/about">ABOUT US</a>
                  </li>
                  <li class="nav-item">
                      <a class="nav-link" href="/project">OUR PROJECT</a>
                  </li>
                  <li class="nav-item">
                      <a class="nav-link" href="#">CLIENTS</a>
                  </li>
                  <li class="nav-item">
                      <a class="nav-link" href="/service">SERVICES</a>
                  </li>
                  <li class="nav-item">
                      <a class="nav-link" href="/contact">CONTACT US</a>
                  </li>
                  <footer class="d-md-none text-white d-flex gap-4">
                      <div class="">Privacy Policy</div>
                      <div class="">Terms & Conditions</div>
                  </footer>
              </ul>
          </div>
      </div>
  </nav>
