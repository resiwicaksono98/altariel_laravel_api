<x-app-layout title="Home page">
    <div class="homepage">
        <!-- Hero -->
        <div class="hero">
            <img src="./assets//hero.png" class="img-fluid hero-img" alt="hero" />
        </div>

        <!-- Desc -->
        <div class="desc text-center">
            <div class="name">JOSH ARCHITECTURE</div>
            <div class="title">Designing for A Better Future</div>
            <hr />
            <div class="description">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                sed do eiusmod tempor incididunt.</div>
        </div>

        <!-- Project -->
        <div class="project">
            <div class="text-center title">Highlight Projects</div>
            <div class="container">
                <div class="row">
                    <div class="col-md-6 my-project">
                        <div class="card text-bg-dark">
                            <img src="./assets/housing.jpg" class="card-img" alt="..." />
                            <div
                                class="card-img-overlay description d-flex flex-column align-items-center justify-content-center gap-2">
                                <h5 class="card-title">Housing</h5>
                                <button type="button" class="btn btn-light">View Project</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 my-project">
                        <div class="card text-bg-dark">
                            <img src="./assets/workplace.jpg" class="card-img" alt="..." />
                            <div
                                class="card-img-overlay description d-flex flex-column align-items-center justify-content-center gap-2">
                                <h5 class="card-title">Workplace</h5>
                                <button type="button" class="btn btn-light">View Project</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 my-project">
                        <div class="card text-bg-dark">
                            <img src="./assets/conventionHall.jpg" class="card-img" alt="..." />
                            <div
                                class="card-img-overlay description d-flex flex-column align-items-center justify-content-center gap-2">
                                <h5 class="card-title">Convention Hall</h5>
                                <button type="button" class="btn btn-light">View Project</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 my-project">
                        <div class="card text-bg-dark">
                            <img src="./assets/building.jpg" class="card-img" alt="..." />
                            <div
                                class="card-img-overlay description d-flex flex-column align-items-center justify-content-center gap-2">
                                <h5 class="card-title">Building</h5>
                                <button type="button" class="btn btn-light">View Project</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Our Client -->
        <div class="our-client text-center">
            <div class="title">Our Clients</div>
            <div class="row image-client d-flex justify-content-center">
                <div class="col-3 col-md-2">
                    <img src="./assets/clients/amazon.png" class="img-fluid rounded-top" alt="" />
                </div>
                <div class="col-3 col-md-2">
                    <img src="./assets/clients/netflix.png" class="img-fluid rounded-top" alt="" />
                </div>
                <div class="col-3 col-md-2">
                    <img src="./assets/clients/spotify.png" class="img-fluid rounded-top" alt="" />
                </div>
                <div class="col-3 col-md-2">
                    <img src="./assets/clients/microsoft.png" class="img-fluid rounded-top" alt="" />
                </div>
                <div class="col-3 col-md-2 mt-4 mt-md-0">
                    <img src="./assets/clients/facebook.png" class="img-fluid rounded-top" alt="" />
                </div>
                <div class="col-3 col-md-2 mt-4 mt-md-0">
                    <img src="./assets/clients/yahoo.png" class="img-fluid rounded-top" alt="" />
                </div>
                <div class="col-3 col-md-2 mt-4 mt-md-5">
                    <img src="./assets/clients/spotify.png" class="img-fluid rounded-top" alt="" />
                </div>
                <div class="d-none d-md-block col-md-2 mt-5">
                    <img src="./assets/clients/microsoft.png" class="img-fluid rounded-top" alt="" />
                </div>
                <div class="d-none d-md-block col-md-2 mt-5">
                    <img src="./assets/clients/facebook.png" class="img-fluid rounded-top" alt="" />
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
