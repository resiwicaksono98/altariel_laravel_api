<x-app-layout title="About">
    <div class="about">
        <!-- Hero -->
        <div class="hero">
            <div class="card text-bg-dark">
                <img src="./assets/about/about-hero.png" class="card-img img-fluid" alt="..." />
                <div
                    class="card-img-overlay description d-flex flex-column align-items-center justify-content-center gap-2">
                    <h5 class="card-title d-none d-md-block">Home / About Us</h5>
                    <h5 class="card-title d-md-none">About Us</h5>
                    <div class="card-text">Designing for A Better Future</div>
                </div>
            </div>
        </div>

        <!-- content 1 -->
        <div class="content">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <img src="./assets/about/image-1.png" class="img-fluid rounded-top" alt="" />
                    </div>
                    <div class="col-md-6 content">
                        <div class="title">Building for The Next Generation</div>
                        <div class="desc mt-4">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
                            tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur
                            adipiscing elit, sed do eiusmod tempor incididunt.</div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Team -->
        <div class="team">
            <div class="title text-center">Meet Our Team</div>
            <div class="container">
                <div class="row">
                    <div class="col-6 col-md-4">
                        <div class="card border-0">
                            <img src="./assets/about/image-2.png" class="img-fluid rounded-top" alt="" />
                            <div class="card-body text-center">
                                <div class="card-title">Louis Amstrong</div>
                                <div class="card-text">Lorem ipsum</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-6 col-md-4">
                        <div class="card border-0">
                            <img src="./assets/about/image-2.png" class="img-fluid rounded-top" alt="" />
                            <div class="card-body text-center">
                                <div class="card-title">Louis Amstrong</div>
                                <div class="card-text">Lorem ipsum</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-6 col-md-4">
                        <div class="card border-0">
                            <img src="./assets/about/image-2.png" class="img-fluid rounded-top" alt="" />
                            <div class="card-body text-center">
                                <div class="card-title">Louis Amstrong</div>
                                <div class="card-text">Lorem ipsum</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-6 col-md-4">
                        <div class="card border-0">
                            <img src="./assets/about/image-2.png" class="img-fluid rounded-top" alt="" />
                            <div class="card-body text-center">
                                <div class="card-title">Louis Amstrong</div>
                                <div class="card-text">Lorem ipsum</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-6 col-md-4">
                        <div class="card border-0">
                            <img src="./assets/about/image-2.png" class="img-fluid rounded-top" alt="" />
                            <div class="card-body text-center">
                                <div class="card-title">Louis Amstrong</div>
                                <div class="card-text">Lorem ipsum</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-6 col-md-4">
                        <div class="card border-0">
                            <img src="./assets/about/image-2.png" class="img-fluid rounded-top" alt="" />
                            <div class="card-body text-center">
                                <div class="card-title">Louis Amstrong</div>
                                <div class="card-text">Lorem ipsum</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Content 2 -->
        <div class="content">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 order-md-2">
                        <img src="./assets/about/image-1.png" class="img-fluid rounded-top" alt="" />
                    </div>
                    <div class="col-md-6 content order-md-1">
                        <div class="title">We Create Everything</div>
                        <div class="desc mt-4">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
                            tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur
                            adipiscing elit, sed do eiusmod tempor incididunt.</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
