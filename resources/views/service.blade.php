<x-app-layout>
    <div class="services">
        <!-- Hero -->
        <div class="hero">
            <div class="card text-bg-dark">
                <img src="./assets/services/service-hero.png" class="card-img img-fluid" alt="..." />
                <div
                    class="card-img-overlay description d-flex flex-column align-items-center justify-content-center gap-2">
                    <h5 class="card-title d-none d-md-block">Home / Service Page</h5>
                    <h5 class="card-title d-md-none">Service Page</h5>
                    <div class="card-text">Designing With the Experts</div>
                </div>
            </div>
        </div>

        <!-- Service -->
        <div class="service">
            <div class="title">Our Services</div>
            <hr class="border my-4 w-75 m-auto border-dark" />
            {{-- Mobile --}}
            <div class="swiper container d-md-none">
                <div class="swiper-wrapper">
                    <!-- Slides -->
                    <div class="swiper-slide">
                        <div class="card text-start p-4">
                            <img class="card-img-rounded rounded-circle" src="./assets/hero.png" alt="Title" />
                            <div class="card-body">
                                <h4 class="card-title mt-2">Lorem Ipsum</h4>
                                <p class="card-text mt-3">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                                    do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="card text-start p-4">
                            <img class="card-img-rounded rounded-circle" src="./assets/hero.png" alt="Title" />
                            <div class="card-body">
                                <h4 class="card-title mt-2">Lorem Ipsum</h4>
                                <p class="card-text mt-3">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                                    do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="card text-start p-4">
                            <img class="card-img-rounded rounded-circle" src="./assets/hero.png" alt="Title" />
                            <div class="card-body">
                                <h4 class="card-title mt-2">Lorem Ipsum</h4>
                                <p class="card-text mt-3">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                                    do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="card text-start p-4">
                            <img class="card-img-rounded rounded-circle" src="./assets/hero.png" alt="Title" />
                            <div class="card-body">
                                <h4 class="card-title mt-2">Lorem Ipsum</h4>
                                <p class="card-text mt-3">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                                    do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="card text-start p-4">
                            <img class="card-img-rounded rounded-circle" src="./assets/hero.png" alt="Title" />
                            <div class="card-body">
                                <h4 class="card-title mt-2">Lorem Ipsum</h4>
                                <p class="card-text mt-3">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                                    do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="card text-start p-4">
                            <img class="card-img-rounded rounded-circle" src="./assets/hero.png" alt="Title" />
                            <div class="card-body">
                                <h4 class="card-title mt-2">Lorem Ipsum</h4>
                                <p class="card-text mt-3">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                                    do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{-- Desktop --}}
            <div class="container d-none d-md-block">
                <div class="row ">
                    <div class="col-md-4">
                        <div class="card text-start p-4">
                            <img class="card-img-rounded rounded-circle" src="./assets/hero.png" alt="Title" />
                            <div class="card-body">
                                <h4 class="card-title mt-2">Lorem Ipsum</h4>
                                <p class="card-text mt-3">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                                    do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card text-start p-4">
                            <img class="card-img-rounded rounded-circle" src="./assets/hero.png" alt="Title" />
                            <div class="card-body">
                                <h4 class="card-title mt-2">Lorem Ipsum</h4>
                                <p class="card-text mt-3">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                                    do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card text-start p-4">
                            <img class="card-img-rounded rounded-circle" src="./assets/hero.png" alt="Title" />
                            <div class="card-body">
                                <h4 class="card-title mt-2">Lorem Ipsum</h4>
                                <p class="card-text mt-3">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                                    do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card text-start p-4">
                            <img class="card-img-rounded rounded-circle" src="./assets/hero.png" alt="Title" />
                            <div class="card-body">
                                <h4 class="card-title mt-2">Lorem Ipsum</h4>
                                <p class="card-text mt-3">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                                    do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card text-start p-4">
                            <img class="card-img-rounded rounded-circle" src="./assets/hero.png" alt="Title" />
                            <div class="card-body">
                                <h4 class="card-title mt-2">Lorem Ipsum</h4>
                                <p class="card-text mt-3">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                                    do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card text-start p-4">
                            <img class="card-img-rounded rounded-circle" src="./assets/hero.png" alt="Title" />
                            <div class="card-body">
                                <h4 class="card-title mt-2">Lorem Ipsum</h4>
                                <p class="card-text mt-3">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                                    do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- If we need pagination -->
            <div class="swiper-pagination"></div>

            <!-- If we need navigation buttons -->
            <div class="swiper-button-prev "></div>
            <div class="swiper-button-next"></div>

        </div>

        <!-- Content -->
        <div class="content">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <img src="./assets/services/image-1.png" class="img-fluid rounded-top" alt="" />
                    </div>
                    <div class="col-md-6 d-flex flex-column desc justify-content-center">
                        <div class="title">From The Scratch</div>
                        <div class="body mt-4">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
                            tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur
                            adipiscing elit, sed do eiusmod tempor incididunt.</div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    @slot('script')
        <script>
            const swiper = new Swiper('.swiper', {
                // Optional parameters
                loop: true,

                // If we need pagination
                pagination: {
                    el: '.swiper-pagination',
                },

                // Navigation arrows
                navigation: {
                    nextEl: '.swiper-button-next',
                    prevEl: '.swiper-button-prev',
                },


            });
        </script>
    @endslot
</x-app-layout>
