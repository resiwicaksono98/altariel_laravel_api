<x-app-layout>

    <div class="project">
        <!-- Hero -->
        <div class="hero">
            <div class="card text-bg-dark">
                <img src="./assets/project/project-hero.png" class="card-img img-fluid" alt="..." />
                <div
                    class="card-img-overlay description d-flex flex-column align-items-center justify-content-center gap-2">
                    <h5 class="card-title d-none d-md-block">Home / All Project</h5>
                    <h5 class="card-title d-md-none">All Project</h5>
                    <div class="card-text">Designing With the Experts</div>
                </div>
            </div>
        </div>

        <!-- Content -->
        <div class="content">
            <div class="container">
                <div class="row d-flex gap-3">
                    <div class="col-12">
                        @foreach ($projects['data'] as $project)
                            <div class="card mb-3 border-0">
                                <div class="row g-0">
                                    <div class="col-md-4">
                                        <img src="{{ $project['main_image'] }}" class="img-fluid rounded-start"
                                            alt="..." />
                                    </div>
                                    <div class="col-md-8 d-flex">
                                        <div class="card-body d-flex flex-column justify-content-center">
                                            <h5 class="card-title">{{ $project['name'] }}</h5>
                                            <p class="card-text">
                                                {{ $project['short_description'] ?? 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ' }}
                                            </p>
                                            <ul class="card-list">
                                                {!! $project['description'] !!}
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>

        <!-- Pagination -->
        <nav class="d-flex justify-content-center mb-5">
            <ul class="pagination">
                <li class="page-item">
                    <a class="page-link" href="#" aria-label="Previous">
                        <span aria-hidden="true">&laquo;</span>
                    </a>
                </li>
                <li class="page-item"><a class="page-link" href="#">1</a></li>
                <li class="page-item"><a class="page-link" href="#">2</a></li>
                <li class="page-item"><a class="page-link" href="#">3</a></li>
                <li class="page-item">
                    <a class="page-link" href="#" aria-label="Next">
                        <span aria-hidden="true">&raquo;</span>
                    </a>
                </li>
            </ul>
        </nav>
    </div>
</x-app-layout>
