<x-app-layout title="Contact">
    <div class="contact">
        <!-- Hero -->
        <div class="hero">
            <div class="card text-bg-dark">
                <img src="./assets/contact/contact-hero.jpg" class="card-img img-fluid" alt="..." />
                <div
                    class="card-img-overlay description d-flex flex-column align-items-center justify-content-center gap-2">
                    <h5 class="card-title d-none d-md-block">Home / Contact Us</h5>
                    <h5 class="card-title d-md-none">About Us</h5>
                    <div class="card-text">Designing for A Better Future</div>
                </div>
            </div>
        </div>

        <div class="container all-content">
            <!-- Content -->
            <div class="content">
                <div class="row">
                    <div class="col-md-6">
                        <img src="./assets/contact/image-1.png" class="img-fluid rounded" alt="" />
                    </div>
                    <div class="col-md-6 d-flex flex-column justify-content-center body-content p-4">
                        <div class="title">Get Touch With US!</div>
                        <div class="body mt-4">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
                            tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur
                            adipiscing elit, sed do eiusmod tempor incididunt.</div>
                        <div class="whatsapp my-4 bg-dark d-flex p-2 gap-2 justify-content-center">
                            <img src="./assets/contact/whatsapp.png" class="img-fluid rounded-top" alt="" />
                            <div class="text-white whatsapp-text">Chat Whatsapp</div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Location -->
            <div class="location">
                <div class="row">
                    <div class="col d-flex flex-column align-items-center">
                        <div class="title my-5">Our Location</div>
                        <img src="./assets/contact/location.png" class="img-fluid rounded-top" alt="" />
                        <div class="address mt-3 mb-4">17th Sesame Street, Upper East High, New York, USA Postal Code
                            127658
                        </div>
                        <button class="btn btn-dark" href="#" role="button">Open Maps</button>
                    </div>
                </div>
            </div>
            <!-- Contact -->
            <div class="contact-us">
                <div class="title text-center">Contact</div>
                <form>
                    <div class="row mt-5 mb-3">
                        <div class="col-6">
                            <input type="text" class="form-control" placeholder="Name" />
                        </div>
                        <div class="col-6">
                            <input type="text" class="form-control" placeholder="Email" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 mb-3">
                            <input type="text" class="form-control" placeholder="Phone Number (Optional)" />
                        </div>
                        <div class="col-12 mb-3">
                            <input type="text" class="form-control" placeholder="Order Number (If Applicable)" />
                        </div>
                        <div class="col-12 mb-3">
                            <div class="form-floating">
                                <textarea class="form-control" placeholder="Leave a comment here" id="floatingTextarea2" style="height: 100px"></textarea>
                                <label for="floatingTextarea2">Your Message</label>
                            </div>
                        </div>
                    </div>
                    <div class="text-center">
                        <a class="btn btn-dark" href="#" role="button">Send Message</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</x-app-layout>
